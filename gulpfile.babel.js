import gulp from 'gulp';
import browserify from 'browserify';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import babelify from 'babelify';
import ejs from 'gulp-ejs';

gulp.task('js', () => {
  browserify({
    entries: ['src/js/common.js']
  })
  .transform(babelify, {presets: ["es2015"]})
  .bundle()
  .pipe(source('build.js'))
  .pipe(buffer())
  .pipe(gulp.dest('template/js'))
});

gulp.task('ejs', () => {
  gulp.src(["src/ejs/*.ejs",'!' + "src/ejs/**/_*.ejs"])
  .pipe(ejs())
  .pipe(gulp.dest("./template"))
})

gulp.task('default', ['js', 'ejs']);
